# part 1
- This script looks for all `files` which their name `ends` with .dolphin.temp 
and are located at `/tmp` and has been modified at least 24 hours ago. Then it will log it
to /tmp/clean.log with every detail such as to whom it belongs to which group it
belong what access mode it has when it was last modified and ... with the current
date appended to its begining 

- This script can be use to delete every specifc temporary file which has been 
modified at least 24 hours ago (because maybe it's not going to be use any more) to prevent 
lack of memory or something like that

# part 2
- It's better to use `/usr/bin/env bash` instead of `/bin/bash`. because env is located at
/usr/bin in every system but bash location maybe in another place. also using
`/usr/bin/env bash` can have it own drawbacks too, as it looks in to env for bash which
can point to a non standard version of bash

- searching for files at / can take a lot of time and doing so at boot time can increace 
boot time drastically. And as it's happening in a cron job it can alson 
affect system performance

- If anyone can change PATH can cause this script to run arbitrary code. for example you can
override find command like this:
`export PATH=/path/to/false/find:$PATH`

```
#!/bin/bash
shutdown now;
```

- It not good to store log at /tmp as there is a spcific place for it /var/log


# part 3
```bash
#!/usr/bin/env bash

PATH=/usr/bin:/bin

adddate() {
    while IFS= read -r line
    do
        echo "$(date) $line"
    done
}

for file in $( find /tmp -type f -mtime +0 -name '*.dolphin.temp')
do
    echo $file
    ls -la $file | adddate >> /var/log/clean.log
done

exit 0

```
