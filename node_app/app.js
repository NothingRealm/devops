
const express = require("express");
const app = express();
const port = 5000;

app.get("/", (req, res) => {
    let today = new Date();
    res.send(`Today: ${today.toLocaleString()} !`);
});
app.listen(port, () => {
    console.log(`Server Started on Port  ${port}`);
});
